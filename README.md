## Intention

This project is an example of using GraphQL on a Spring Boot application.

It's based on the tutorial at the [official site](https://www.graphql-java.com/tutorials/getting-started-with-spring-boot/).

but with the following changes:

1. A Configuration class is used to declare the GraphQL Bean instead of a Component class, avoiding the PostConstruct annotation.
1. The GraphQL Schema is loaded directly as an InputStream, avoiding the (unnecessary) conversion to String.
1. The building of the RuntimeWiring, being considered central, is extracted to a RuntimeWiringProvider Component class.
1. For clarity, definitions of DataFetchers are also extracted to a similar DataFercherProvider Component class.

The main files are the [configuration file](src/main/java/me/d3249/example/graphql/configuration/GraphQLConfiguration.java) 
and the providers for [RuntimeWiring](src/main/java/me/d3249/example/graphql/graphql/RuntimeWiringProvider.java) and
[DataFercher<T>](src/main/java/me/d3249/example/graphql/graphql/DataFetcherProvider.java).


## The model

This simple exaple has two entity classes: Author and Book. A Book has an Author. There's a service method to recover 
all books by a given an author Id.

## The Service layer

Service layer is simulated in a single class. The inner working uses a Map as a database, which is initialized with a few
books.

The creation of a book is just a dumb stub, it creates a new Author for each Book, in a real life application there will
be some logic involved, but for this example it's out of scope.

## Other libraries

This example uses lombok to reduce some code verbosity on the model, but the main files are written in plain Java+Spring.