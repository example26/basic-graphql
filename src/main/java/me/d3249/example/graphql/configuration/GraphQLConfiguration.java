package me.d3249.example.graphql.configuration;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import me.d3249.example.graphql.graphql.RuntimeWiringProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;

@Configuration
public class GraphQLConfiguration {

    private final RuntimeWiringProvider runtimeWiringProvider;

    private String schemaPath;

    @Autowired
    public GraphQLConfiguration(RuntimeWiringProvider runtimeWiringProvider) {
        this.runtimeWiringProvider = runtimeWiringProvider;
    }

    @Bean
    public GraphQL graphQL() {
        return GraphQL.newGraphQL(buildGraphQLSchema()).build();
    }


    private GraphQLSchema buildGraphQLSchema() {
        TypeDefinitionRegistry registry = buildTypeDefinitionRegistry();
        RuntimeWiring wiring = runtimeWiringProvider.getRuntimeWiring();

        return new SchemaGenerator().makeExecutableSchema(registry, wiring);
    }

    private TypeDefinitionRegistry buildTypeDefinitionRegistry() {
        InputStream schemaDefinition = ClassLoader.getSystemResourceAsStream(schemaPath);
        return new SchemaParser().parse(schemaDefinition);
    }

    @Value("${graphql.schemaPath}")
    public void setSchemaPath(String schemaPath) {
        this.schemaPath = schemaPath;
    }
}
