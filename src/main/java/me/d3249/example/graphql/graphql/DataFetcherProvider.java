package me.d3249.example.graphql.graphql;

import graphql.schema.DataFetcher;
import me.d3249.example.graphql.model.Author;
import me.d3249.example.graphql.model.Book;
import me.d3249.example.graphql.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

@Component
public class DataFetcherProvider {
    private final BookService service;

    @Autowired
    public DataFetcherProvider(BookService service) {
        this.service = service;
    }

    public DataFetcher<Collection<Book>> allBooks() {
        return env -> service.allBooks();
    }

    public DataFetcher<Optional<Book>> bookByIsbn() {
        return env -> {
            String isbn = env.getArgument("isbn");
            return service.bookByIsbn(isbn);
        };
    }

    public DataFetcher<Collection<Book>> booksByPublicationDateRange() {
        return env -> {
            int desde = env.getArgument("from");
            int hasta = env.getArgument("to");

            return service.booksPublishedBetween(desde, hasta);
        };
    }

    public DataFetcher<Book> registerBook() {
        return env -> {
            Map<String, Object> entrada = env.getArgument("newBook");

            return service.addBook((String) entrada.get("isbn"),
                    (String) entrada.get("title"),
                    (Integer) entrada.get("publishYear"),
                    (String) entrada.get("authorName"));
        };
    }

    public DataFetcher<Collection<Book>> booksByAuthorId() {
        return environment -> {
            Author author = environment.getSource();
            return service.booksByAuthor(author);
        };
    }
}
