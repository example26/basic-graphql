package me.d3249.example.graphql.graphql;

import graphql.schema.idl.RuntimeWiring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

@Component
public class RuntimeWiringProvider {

    private final RuntimeWiring runtimeWiring;


    @Autowired
    public RuntimeWiringProvider(DataFetcherProvider provider) {
        this.runtimeWiring = RuntimeWiring
                .newRuntimeWiring()

                .type(newTypeWiring("Query")
                        .dataFetcher("books", provider.allBooks())
                        .dataFetcher("bookByIsbn", provider.bookByIsbn())
                        .dataFetcher("publishedBetween", provider.booksByPublicationDateRange())
                )

                .type(newTypeWiring("Mutation")
                        .dataFetcher("new", provider.registerBook())
                )

                .type(newTypeWiring("Author")
                        .dataFetcher("books", provider.booksByAuthorId())
                )

                .build();
    }

    public RuntimeWiring getRuntimeWiring() {
        return runtimeWiring;
    }
}
