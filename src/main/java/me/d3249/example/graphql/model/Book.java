package me.d3249.example.graphql.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Book {

    private final String isbn;
    private final String title;
    private final int publishYear;
    private final Author author;

}
