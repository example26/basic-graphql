package me.d3249.example.graphql.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Author {
    private final int id;
    private final String name;
}
