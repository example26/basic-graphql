package me.d3249.example.graphql.service;

import me.d3249.example.graphql.model.Author;
import me.d3249.example.graphql.model.Book;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class BookService {


    private static Map<String, Book> database = new HashMap<>();

    static {

        var martin = new Author(1, "George R. R. Martin");
        var tolkien = new Author(2, "J. R. R. Tolkien");
        var card = new Author(3, "Orson Scott Card");

        var book1 = new Book("ISBN-001", "A game of thrones", 1996, martin);
        var book2 = new Book("ISBN-002", "A clash of kings", 1999, martin);
        var book3 = new Book("ISBN-003", "A storm of swords", 2000, martin);
        var book4 = new Book("ISBN-004", "A feast for crows", 2005, martin);
        var book5 = new Book("ISBN-005", "A dance with dragons", 2011, martin);

        var book6 = new Book("ISBN-006", "The fellowship of the ring", 1954, tolkien);
        var book7 = new Book("ISBN-007", "The two towers", 1954, tolkien);
        var book8 = new Book("ISBN-008", "The return of the king", 1955, tolkien);

        var book9 = new Book("ISBN-009", "Ender's game", 1985, card);
        var book10 = new Book("ISBN-010", "Speaker of the dead", 1986, card);
        var book11 = new Book("ISBN-011", "Ender  xenocide", 1991, card);
        var book12 = new Book("ISBN-012", "Children of the mind", 1996, card);

        database.put(book1.getIsbn(), book1);
        database.put(book2.getIsbn(), book2);
        database.put(book3.getIsbn(), book3);
        database.put(book4.getIsbn(), book4);
        database.put(book5.getIsbn(), book5);
        database.put(book6.getIsbn(), book6);
        database.put(book7.getIsbn(), book7);
        database.put(book8.getIsbn(), book8);
        database.put(book9.getIsbn(), book9);
        database.put(book10.getIsbn(), book10);
        database.put(book11.getIsbn(), book11);
        database.put(book12.getIsbn(), book12);

    }


    public Collection<Book> allBooks() {
        return database.values();
    }

    public Optional<Book> bookByIsbn(String isbn) {
        return Optional.ofNullable(database.get(isbn));
    }

    public Collection<Book> booksByAuthor(Author author) {
        return allBooks().stream()
                .filter(l -> author.equals(l.getAuthor()))
                .collect(Collectors.toList());
    }

    public Collection<Book> booksPublishedBetween(int from, int to) {
        return allBooks().stream()
                .filter(l -> l.getPublishYear() >= from && l.getPublishYear() <= to)
                .collect(Collectors.toList());
    }

    public Book addBook(String isbn, String title, int publishYear, String autorName) {
        var libro = new Book(isbn, title, publishYear, new Author(database.size() + 1, autorName));

        database.put(isbn, libro);

        return libro;
    }
}
