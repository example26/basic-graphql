package me.d3249.example.graphql;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GqlDemoApplicationTests {

    @Test
    void contextLoads() {
        Assertions.assertTrue(true, "Canary test is failing.");
    }

}
